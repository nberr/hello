FLAGS = -Wall -Werror
DFLAGS = $(FLAGS) -g --coverage

default: hello

debug: hello_d

test: 
	./hello_d >hello.out
	gcovr -r . 
	gcovr -r . --html --html-details -o report.html

hello_d.o: hello.c
	gcc $(DFLAGS) -c hello.c -o hello_d.o

hello.o: hello.c
	gcc $(FLAGS) -c hello.c

hello_d: hello_d.o
	gcc $(DFLAGS) -o hello_d hello_d.o

hello: hello.o
	gcc $(FLAGS) -o hello hello.o

clean: 
	rm -f hello hello_d *.o