# yet another hello world example

This repo is ment for testing CI functionality with the famous helloworld program

[![pipeline status](https://git.rwth-aachen.de/nberr/hello/badges/master/pipeline.svg)]()
[![pipeline status](https://git.rwth-aachen.de/nberr/hello/badges/master/coverage.svg)]()

![](images/coverage.png)